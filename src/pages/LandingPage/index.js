import React, { useEffect, useState } from 'react';
import { Box, Grid, Container, Card, CardContent, Typography,  CardHeader, Avatar, IconButton } from '@mui/material';
import { red } from '@mui/material/colors';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import CardMedia from '@mui/material/CardMedia';
import axios from 'axios';
import createTheme from '../../components/CreateTheme';
import PropTypes from 'prop-types';

const dataAnggota = [
    {
        id: 1,
        image: 'https://images.unsplash.com/photo-1687721449660-f902b9d22624?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=774&q=80',
        name: 'taufiiq',
        address: 'Indonesia'
    },
    {
        id: 2,
        image: 'https://images.unsplash.com/photo-1687721449660-f902b9d22624?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=774&q=80',
        name: 'hakim',
        address: 'spain'
    }
]

const LandingPage = () => {
    const [data, setData] = useState([]);
    const apiUrl = process.env.REACT_APP_BASE_URL;
    
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(apiUrl) // Ganti dengan URL dan jalur yang sesuai dengan konfigurasi Mockoon Anda
        const responseData = await response.json();
        setData(responseData);
      } catch (error) {
        console.error("Error fetching data",error);
      }
    };

    console.log(process.env.REACT_APP_BASE_URL)

    fetchData();
  }, [apiUrl]);

  
  const theme = createTheme();

  return (
    <Container >
     
      <Grid container sx={{ marginTop: '24px' }}>
        <Grid xs={12} md={8}>
          <Box sx={{ border: '5px solid red', borderColor: theme.colors.primary, height: '300vh', marginRight: '20px' }}>
            <Grid container>
              {data.map((item) => (
                <Grid xs={6}>
                  <Card sx={{ minWidth: 275, marginX: '20px', marginTop: '20px' }}>
                    <CardHeader
                      avatar={
                        <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
                          R
                        </Avatar>
                      }
                      action={
                        <IconButton aria-label="settings">
                          <MoreVertIcon />
                        </IconButton>
                      }
                      title={item.title}
                      subheader={item.date}
                    />
                    <CardMedia component="img" height="100" image={item.img} alt="Paella dish" />
                    <CardContent>
                      <Typography variant="body2" color="text.secondary">
                        {item.description}
                      </Typography>
                    </CardContent>
                  </Card>
                </Grid>
              ))}
            </Grid>
          </Box>
        </Grid>
        <Grid xs={12} md={4}>
                    <Box sx={{ border: '5px solid  ${theme.colors.secondary}', height: '50vh' }}>
                        <Box sx={{ padding: '20px' }}>
                            {dataAnggota.map((item, index) => {
                                return (
                                    <Box sx={{ paddingTop: index !== 0 ? '10px' : '' }}>
                                        <Grid container>
                                            <Grid xs={3}>
                                                <img src={item.img} alt="image" height={60} width={60} style={{ borderRadius: '100%' }} />
                                            </Grid>
                                            <Grid xs={8}>
                                                <Typography color={theme.colors.primary} fontWeight={500} fontSize={'20px'}>
                                                    {item.name}
                                                </Typography>
                                                <Typography>
                                                    {item.address}
                                                </Typography>
                                            </Grid>
                                        </Grid>
                                    </Box>
                                )
                            }
                            )}
                        </Box>
                    </Box>
                </Grid>
      </Grid>
    </Container>
  );
};

LandingPage.propTypes = {
    apiUrl: PropTypes.string.isRequired,
  };

export default LandingPage