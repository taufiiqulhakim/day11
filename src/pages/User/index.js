import React, { useState } from 'react';
import {Typography, Button, Box, TextField } from '@mui/material';
import UserFormDialog from "../../components/UserFormDialog"


const User = () => {
  const [openDialog, setOpenDialog] = useState(false);
  const [users, setUsers] = useState([]); 
  const [editUserId, setEditUserId] = useState(null);
  const [searchTerm, setSearchTerm] = useState("");

  const handleAddUser = () => {
    setOpenDialog(true);
    setEditUserId(null);
  };

  const handleEditUser = (userId) => {
    setOpenDialog(true);
    setEditUserId(userId);
  };

  const handleCloseDialog = () => {
    setOpenDialog(false);
    setEditUserId(null);
  };

  const handleSaveUser = (newUser) => {
    setUsers([...users, newUser]);
    if (editUserId !== null) {
      // Editing existing user
      const updatedUsers = users.map((user) =>
        user.id === editUserId ? { ...user, ...newUser } : user
      );
      setUsers(updatedUsers);
    } else {
      // Adding new user
      newUser.id = Math.random(); // Assign a unique ID to the new user
      setUsers([...users, newUser]);
    }
    setOpenDialog(false);
    setEditUserId(null);
  };

  const handleSearchChange = (event) => {
    setSearchTerm(event.target.value);
  };

  const filteredUsers = users.filter((user) =>
    user.name.toLowerCase().includes(searchTerm.toLowerCase())
  );

  return (
    <Box sx={{ backgroundColor: 'white', minHeight: '100vh' }}>
      <UserFormDialog
        open={openDialog}
        handleClose={handleCloseDialog}
        handleSave={handleSaveUser}
        editUserId={editUserId}
      />

      <Box sx={{ m: '6px'}}>
        <TextField
          label="Search"
          variant="outlined"
          value={searchTerm}
          onChange={handleSearchChange}
          fullWidth
          sx={{ maxWidth: '200px', }}
        />
      </Box>

      {/* {filteredUsers.length > 0 ? (
        filteredUsers.map((item) => ( */}
      {users.length > 0 ? (
        users.map((item) => (
          <Box
            key={item.id}
            sx={{
              border: '1px solid black',
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
              paddingX: '5px',
              paddingY: '5px'
            }}
          >
            <Box>
              <Typography variant="h6" component="div" sx={{ mx: 'auto' }}>
                {item.name}
              </Typography>
              <Typography variant="h6" component="div" sx={{ mx: 'auto' }}>
                {item.address}
              </Typography>
            </Box>
            <Box>
              <Typography variant="h6" component="div" sx={{ mx: 'auto' }}>
                {item.hobby}
              </Typography>
              <Button
                color="inherit"
                sx={{
                  backgroundColor: 'aqua',
                  borderRadius: '20px',
                  textTransform: 'none'
                }}
                onClick={() => handleEditUser(item.id)}
              >
                Edit
              </Button>
            </Box>
          </Box>
        ))
      ) : (
        <Box
          sx={{
            display: 'flex',
            height: '80vh',
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column'
            }}
          >
            <Typography
              variant="h1"
              component="div"
              sx={{
                marginBottom: '8px',
                fontSize: '25rem',
                color: 'orange',
                fontFamily: 'sans-serif',
                mx: 'auto'
              }}
            >
              0
            </Typography>
            <Typography
              variant="h1"
              component="div"
              sx={{
                marginTop: '-90px',
                fontSize: '10rem',
                color: 'red',
                fontStyle: 'italic',
                fontFamily: 'sans-serif',
                mx: 'auto'
              }}
            >
              User
            </Typography>
          </Box>
        </Box>
      )}

      <Button variant="contained" onClick={handleAddUser}>
        Add User
      </Button>
    </Box>
  );
};

export default User;
