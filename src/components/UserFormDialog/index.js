// UserFormDialog.js
import React, { useState } from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, TextField, Grid, Button } from '@mui/material';

const UserFormDialog = ({ open, handleClose, handleSave }) => {
  const [name, setName] = useState('');
  const [address, setAddress] = useState('');
  const [hobby, setHobby] = useState('');

  const handleSaveUser = () => {
    const newUser = {
      name: name,
      address: address,
      hobby: hobby
    };

    handleSave(newUser);

    setName('');
    setAddress('');
    setHobby('');

    handleClose();
  };

  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogTitle sx={{ textAlign: 'center' }}>Add User</DialogTitle>
      <DialogContent>
        <TextField
          label="Name"
          value={name}
          onChange={(e) => setName(e.target.value)}
          fullWidth
          margin="normal"
        />
        <TextField
          label="Address"
          value={address}
          onChange={(e) => setAddress(e.target.value)}
          fullWidth
          margin="normal"
        />
        <TextField
          label="Hobby"
          value={hobby}
          onChange={(e) => setHobby(e.target.value)}
          fullWidth
          margin="normal"
        />
      </DialogContent>
      <DialogActions sx={{ justifyContent: 'center' }}>
        <Grid container justifyContent="center">
          <Grid item>
            <Button onClick={handleSaveUser} color="primary">
              Save
            </Button>
          </Grid>
        </Grid>
      </DialogActions>
    </Dialog>
  );
};

export default UserFormDialog;
