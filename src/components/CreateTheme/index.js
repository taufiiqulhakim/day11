const createTheme = () => {
    let primaryColor;
  
    if (process.env.REACT_APP_ENVIRONMENT === 'local') {
      primaryColor = process.env.REACT_APP_COLOR_PRIMARY_LOCAL || '#ff0000'; // Default primary color untuk local environment (misalnya merah)
    } else if (process.env.REACT_APP_ENVIRONMENT === 'alpha') {
      primaryColor = process.env.REACT_APP_COLOR_PRIMARY_ALPHA || '#00ff00'; // Default primary color untuk alpha environment (misalnya hijau)
    } else if (process.env.REACT_APP_ENVIRONMENT === 'beta') {
      primaryColor = process.env.REACT_APP_COLOR_PRIMARY_BETA || '#0000ff'; // Default primary color untuk beta environment (misalnya biru)
    } else {
      primaryColor = '#ff0000'; // Default primary color jika environment tidak cocok dengan yang ditentukan
    }
  
    const theme = {
      colors: {
        primary: primaryColor,
        // ...properti warna lainnya...
      },
      // ...properti tema lainnya...
    };
  
    return theme;
  };
  
  export default createTheme;
  