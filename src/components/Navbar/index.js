import React, { useState, useContext } from 'react';
import { AppBar, Toolbar, Typography, Button, Box, Link } from '@mui/material';
import UserFormDialog from '../UserFormDialog';
import { Link as RouterLink } from 'react-router-dom';
import User from '../../pages/User';
import { CommonContext } from '../../context/CommonContext';

const Navbar = ({ handleAddUser }) => {
  const { commonState } = useContext(CommonContext);
  const [openDialog, setOpenDialog] = useState(false);
  const [userData, setUserData] = useState(null);
  const [users, setUsers] = useState([]); 

  const handleOpenDialog = () => {
    setOpenDialog(true);
  };

  const handleCloseDialog = () => {
    setOpenDialog(false);
  };
  const handleSaveUser = (newUser) => {
    setUsers([...users, newUser]);
    setUserData(newUser);
    handleCloseDialog();
  };

  return (
    <Box>
       <nav style={{ backgroundColor: commonState.color }}>
        <h2>{commonState.title}</h2>
      </nav>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            <Box sx={{ display: 'flex', alignItems: 'center', gap: '15px' }}>
              My Apps
              <Box sx={{ display: 'flex', alignItems: 'center', gap: '15px' }}>
                <Link component={RouterLink} to="/about" color="inherit" underline="none">
                  About
                </Link>
                <Link component={RouterLink} to="/hobby" color="inherit" underline="none">
                  Hobby
                </Link>
              </Box>
            </Box>
          </Typography>
          <Button
            color="inherit"
            sx={{ backgroundColor: 'aqua', borderRadius: '20px', textTransform: 'none' }}
            onClick={handleOpenDialog}
          >
            Add Userw
          </Button>
          {/* <Button variant="contained" onClick={handleAddUser}>
        Add User
      </Button> */}
        </Toolbar>
      </AppBar>
      <UserFormDialog 
        open={openDialog} 
        handleClose={handleCloseDialog} 
        handleSave={handleSaveUser} 
      />
      
    </Box>
    
  );
};

export default Navbar;
