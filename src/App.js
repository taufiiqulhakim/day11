import React, { useState, useEffect} from 'react'
import Axios from 'axios'
import {BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom'
import Home from './pages/Home/Home'
import About from './pages/About'
import Detail from './pages/Detail'
import Example from './pages/Example'
import User from './pages/User'
import './App.css';
import Navbar from './components/Navbar'
import List from './pages/List'
import LandingPage from './pages/LandingPage'
import { CommonProvider } from './context/CommonContext'

function App() {
	let token = null
	const [data, setData] = useState([])


	useEffect(() => {
		Axios.get('https://jsonplaceholder.typicode.com/todos/1').then((res) => setData(res.data))
	}, [])

	const handleAddUser = (newUser) => {
		// Lakukan logika penyimpanan data atau tindakan lainnya di sini
		console.log(newUser);
	  };
	

  
	
  return (
	<CommonProvider>

    <Router>
		<Navbar handleAddUser={handleAddUser} />
			<Switch>
				<Route exact path="/">
					{token ? <Redirect to="/detail" /> :	<Home/>}
				</Route>
				<Route exact path="/about">
						<About/>
				</Route>
				<Route exact path="/list">
						<List/>
				</Route>
				<Route exact path="/detail">
						<Detail/>
				</Route>
				<Route exact path="/detail/:id">
						<Detail/>
				</Route>
				<Route exact path="/example">
						<Example/>
				</Route>
				<Route exact path="/landingpage">
						<LandingPage/>
				</Route>
				<Route path="/user">
          			<User handleAddUser={handleAddUser} />
        		</Route>
        		<Redirect to="/" />
				<Route exact path="/user/:id">
						<Detail/>
				</Route>
			</Switch>
		</Router>
	</CommonProvider>
  );
}

export default App;

// <div className="App">
//   {/* <header className="App-header">
	
// 		<h1>Krisna Firdaus</h1>
// 		<h2>Andi Muhammad</h2>

		
		
//   </header>

// 	<div className='btn-all'>
// 		<button>Reset</button>
// 		<button>Stop</button>
// 		<button>Start</button>
// 	</div> */}
// 	{/* <Home dataProps={data} /> */}
// 	<Example />

// </div>