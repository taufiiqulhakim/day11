import React, { createContext, useState } from 'react';

export const CommonContext = createContext();

export const CommonProvider = ({ children }) => {
  const [commonState, setCommonState] = useState({ title: '', color: '' });

  const updateCommonState = (newState) => {
    setCommonState((prevState) => ({ ...prevState, ...newState }));
  };

  return (
    <CommonContext.Provider value={{ commonState, updateCommonState }}>
      {children}
    </CommonContext.Provider>
  );
};
